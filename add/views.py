from django.shortcuts import render, redirect
from django.urls import reverse
from urllib.parse import urlencode
from django.http import JsonResponse
from django.contrib import messages
import json
from django.http import HttpResponse

from .forms import ResepModelForm, KomentarModelForm
from add.models import Resep, Komentar

def add(request):
    template_name = 'add/add.html'
    if request.method == 'GET':
        resepform = ResepModelForm(request.GET or None)
    elif request.method == 'POST':
        resepform = ResepModelForm(request.POST, request.FILES)
        if resepform.is_valid():
            resep = resepform.save(commit=False)
            resep.penulis = request.user.username
            resep.save()
            return redirect('search:idx')
    
    return render(request, template_name, {
        'resepform': resepform,
    })

def detail(request, detail_id):
    resep_detail = Resep.objects.get(id=detail_id)
    allkomentar = resep_detail.komentar.all()
    context = {}
    if request.method == 'POST':
        if request.is_ajax():
            if request.user.is_authenticated:
                komentarform = KomentarModelForm(request.POST)
                if komentarform.is_valid():
                    komentar = komentarform.save(commit=False)
                    komentar.name = request.user.username
                    komentar.message = komentarform.cleaned_data['message']
                    komentar.time = "1 Januari 2021"
                    komentar.resep = resep_detail
                    komentar.save()
                    return JsonResponse({'success': True})
            return JsonResponse({'success': False})
        return HttpResponse("Ajax is failed")
    else:
        komentarform = KomentarModelForm(request.GET or None)
        context = {
        'resep_detail':resep_detail,
        'allkomentar' : allkomentar,
        'komentarform' : komentarform,
        }
        return render(request, 'resep-details.html', context)

def get_JSON(request,detail_id):
    resep_detail = Resep.objects.get(id=detail_id)
    data = {
        'data':[i.get_dict() for i in resep_detail.komentar.all()[::-1][:5]],
    }
    return JsonResponse(data=data)
