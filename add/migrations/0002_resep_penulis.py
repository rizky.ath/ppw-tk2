# Generated by Django 3.1.2 on 2020-12-23 03:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('add', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='resep',
            name='penulis',
            field=models.CharField(default='anonim', max_length=255),
            preserve_default=False,
        ),
    ]
