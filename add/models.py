from django.db import models


class Resep(models.Model):
    foto = models.ImageField(upload_to='images')
    nama = models.CharField(max_length=255)
    deskripsi = models.CharField(max_length=255)
    porsi = models.CharField(max_length=255)
    durasi = models.CharField(max_length=255)
    bahan = models.TextField()
    langkah = models.TextField()
    penulis = models.CharField(max_length=255)

    def __str__(self):
        return self.nama

class Komentar(models.Model):
    name = models.CharField(max_length=50, blank=True, default="anonymous", null=True)
    time = models.DateTimeField(auto_now_add=True, blank=True,  null=True)
    message = models.CharField(max_length=300, blank=False, default="",  null=True)

    resep = models.ForeignKey(Resep, on_delete=models.CASCADE, related_name="komentar", null=True)

    def get_dict(self):
        return {
            'name':self.name,
            'message':self.message,
            'time':self.time
        }
