from django.contrib import admin

# Register your models here.
from .models import Resep, Komentar

class ResepAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)

admin.site.register(Resep, ResepAdmin)
admin.site.register(Komentar,)
