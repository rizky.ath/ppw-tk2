from django.urls import path

from add.views import add, detail, get_JSON
from add import views

app_name = 'add'

urlpatterns = [
    path('', add, name='add'),
    path('detail/<int:detail_id>/', detail, name='detail'),
    path('detail/json/<int:detail_id>', get_JSON, name='json'),

]
