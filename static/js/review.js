$(document).ready(function(){ 
  $(window).on('scroll', function () {
    if ($(this).scrollTop() > 100) { 
      $('#back-to-top').fadeIn(); 
    } else { 
      $('#back-to-top').fadeOut(); 
    } 
  }); 
  $('#back-to-top').click(function(){ 
    $("html, body").animate({ scrollTop: 0 }, 600); 
    return false; 
  }); 
});

$('.button-up').click(function(){
  var selectedCard = $(this).parent().parent().parent();
  selectedCard.insertBefore(selectedCard.prev());
  console.log("up")
})

$('.button-down').click(function(){
  var selectedCard = $(this).parent().parent().parent();// buat nampilin biar dia dropdown accordionnya ke bawah
  selectedCard.insertAfter(selectedCard.next());//nyisipin selectedCard ke bawah
  console.log("down") // buat urutan naik turun accordion
})

$(document).ready(function(){
  showTestimonial()

  $("#testi-submit").click(function(e){
    e.preventDefault();
      submitForm();
  })
})

function showTestimonial() {
  var dataRes = ""
  $.ajax({
    url : "/review/json",
    error: function(){
        alert("Something went wrong, try again.")
    },
    success: function(data){
      console.log(data);

      $('#ulasan-result').empty();
            if(typeof (data.data) === 'undefined'){
                alert("No review found");
            }
      for(var counter=0; counter<data.data.length; counter++){
        dataRes += '<div class="col-lg-12">';
				dataRes += '<div class="card-deck"></div>';
        dataRes += '<div class="card">';
        dataRes += '<div class="card-body">';
        dataRes += '<h4 class="card-title">' + data.data[counter].name + '</h4>';
        dataRes += '<p class="card-text">'+ data.data[counter].message +'</p>';
        dataRes += '</div>';
        dataRes += '<div class="card-footer">';
        var date = convertTime(data.data[counter].time);
        dataRes += '<small class="text-muted">' + date + '</small>';
        dataRes += '</div>';
        dataRes += '</div>';
        dataRes += '</div>';
      }
      console.log(dataRes);
      $("#ulasan-result").append(dataRes);
    },
  })
}

  

function convertTime(process) {
  var dayArr = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
        var monthArr = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        var timeObj = new Date(process);
        var year = timeObj.getFullYear();
        var month = monthArr[timeObj.getMonth()];
        var date = timeObj.getDate();
        var day = dayArr[timeObj.getDay()];
        var hour = timeObj.getHours();
        if(hour < 10){
            hour = "0" + hour;
        }
        var minute = timeObj.getMinutes();
        if(minute < 10){
            minute = "0" + minute;
        }
    
        console.log(typeof(minute));
        return day + ", " + date + " " + month + " " + year + " | " + hour + ":" + minute;
    }

    function submitForm(){
      $.ajax({
          type: "POST",
          // url : "/review/json/",
          data: $("form#form-ulasan").serialize(),
          error: function(){
              alert("Something went wrong, try again.")
          },
          success: function(data){
              console.log(data)
              if (data.success === true) {
                  showTestimonial();
                  window.setTimeout(function() {
                      alert("Success!");
                  }, 200);
              } else if (data.success === false) {
                  alert("Failed")
              }
          },
          
      })
  }
  


