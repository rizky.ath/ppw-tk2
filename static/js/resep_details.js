$(document).ready(function(){
    showKomentar()
  
    $("#komen-submit").click(function(e){
      e.preventDefault();
        submitForm();
    })
})

function showKomentar() {
    var dataRes = ""
    var resep_id = document.getElementById('komentar').getAttribute('url');
    $.ajax({
      url : resep_id,
      error: function(){
          alert("Something went wrong, try again.")
      },
      success: function(data){
        console.log(data);
  
        $('#komentar').empty();
        if(typeof (data.data) === 'undefined'){
                  alert("No review found");
        }
        for(var counter=0; counter<data.data.length; counter++){
          dataRes += '<p><b>' + data.data[counter].name + '</b> &nbsp';
          var date = convertTime(data.data[counter].time);
          dataRes += '<small class="text-muted">' + date + '</small><br>';
          dataRes += data.data[counter].message +'</p>';
        }
        console.log(dataRes);
        $("#komentar").append(dataRes);
      },
    })
}

function convertTime(process) {
    var dayArr = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
          var monthArr = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
          var timeObj = new Date(process);
          var year = timeObj.getFullYear();
          var month = monthArr[timeObj.getMonth()];
          var date = timeObj.getDate();
          var day = dayArr[timeObj.getDay()];
          var hour = timeObj.getHours();
          if(hour < 10){
              hour = "0" + hour;
          }
          var minute = timeObj.getMinutes();
          if(minute < 10){
              minute = "0" + minute;
          }
      
          console.log(typeof(minute));
          return day + ", " + date + " " + month + " " + year + " | " + hour + ":" + minute;
}

function submitForm(){
    $.ajax({
        type: "POST",
        data: $("form#commentrespondform").serialize(),
        error: function(){
            alert("Something went wrong, try again.")
        },
        success: function(data){
            console.log(data)
            if (data.success === true) {
                showKomentar();
                window.setTimeout(function() {
                    alert("Success!");
                }, 200);
            } else if (data.success === false) {
                alert("Failed")
            }
        },
        
    })
}