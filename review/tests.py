from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from django.apps import apps
from .models import Ulasan
from .forms import FormUlasan
from .views import ulasan, listReview, logIn, signUp, logOut
from .apps import ReviewConfig
from django.contrib.auth.models import User
from .views import logOut, signUp, logIn
import json

class UrlsTest(TestCase):
    def setUp(self):
        self.listReview = reverse("review:listReview")
        self.review = reverse("review:ulasan")

    def test_listReview_use_right_function(self):
        found = resolve(self.listReview)
        self.assertEqual(found.func, listReview)

    def test_ulasan_use_right_function(self):
        found = resolve(self.review)
        self.assertEqual(found.func, ulasan)


class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.listReview = reverse("review:listReview")
        self.ulasan = reverse("review:ulasan")

    def test_GET_listReview(self):
        response = self.client.get(self.listReview)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'stuff.html')


class UlasanlHTMLTest(TestCase):
    def setUp(self):
        User.objects.create_user("zahra", None, "zahra")

    def test_text(self):
        response = self.client.get('/review/')

        # self.assertIn('Silakan Log In terlebih dahulu ya untuk memberi ulasan!', response.content.decode())

        self.client.login(username="zahra", password="zahra")
        response = self.client.get('/review/ulasan')
        self.assertIn('Message:', response.content.decode())

    def test_submit_button(self):
        response = self.client.get('/review/ulasan')
        self.assertNotIn('submit', response.content.decode())

        self.client.login(username="zahra", password="zahra")
        response = self.client.get('/review/ulasan')
        self.assertIn('submit', response.content.decode())

    def test_form(self):
        self.client.login(username="zahra", password="zahra")
        response = self.client.get('/review/ulasan')
        self.assertIn('<form', response.content.decode())
        self.assertIn('<input', response.content.decode())


    def test_urls_login_and_signup_is_exist(self):
        response_login = self.client.get('/review/login/')
        response_signup = self.client.get('/review/signup/')
        self.assertEqual(response_login.status_code, 200)
        self.assertEqual(response_signup.status_code, 200)

    def test_GET_logOut(self):
        response = self.client.get('/review/logout/')
        self.assertEqual(response.status_code, 302)

class UlasanViewsTest(TestCase):
    def setUp(self):
        User.objects.create_user("zahra", None, "zahra")
    
    def test_form_valid(self):
        self.client.login(username="zahra", password="zahra")

        headers = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        response = self.client.post('/review/ulasan', **headers, data={
            'message': 'hello',
        })
        self.assertTrue(Ulasan.objects.filter(message="hello").exists())

    def test_form_invalid(self):
        headers = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        response = self.client.post('/testimonial/', **headers, data={
            'message': 'hello',
        })
        self.assertFalse(Ulasan.objects.filter(message="hello").exists())


class UlansanModelTest(TestCase):
    def setUp(self):
        data = Ulasan(
            name="zahra",
            message="tes",
        )
        data.save()

    def test_get_JSON_correct(self):
        response = self.client.get("/review/json/")
        ulasan = Ulasan.objects.all()[::-1][:5]
        self.assertEqual(len(json.loads(response.content)['data']), len(ulasan))

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(ReviewConfig.name, 'review')
        self.assertEqual(apps.get_app_config('review').name, 'review')

    
class TestLoginSignup(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('uname', password='pass', is_staff=False)
        self.login_response_success = Client().post('/review/login/',{
            'username':'uname', 'password':'pass'
            })
        self.login_response_fail = Client().post('/review/login/', {
            'username':'uname', 'password':'password'
            })
        self.signup_response_success = Client().post('/review/signup/',{
            'first_name':'coba', 'last_name':'dong', 'username':'cobadong',
            'password1':'palepale', 'password2':'palepale'
            })
        self.signup_response_fail = Client().post('/review/signup/',{
            'first_name':'coba', 'last_name':'dong', 'username':'cobadong',
            'password1':'palepale', 'password2':'palepela'
            })

    def test_user_ada(self):
        self.assertEqual(User.objects.count(), 2)

    def test_signup(self):
        self.assertEqual(self.signup_response_success.status_code, 302)
        self.assertEqual(self.signup_response_fail.status_code, 200)

    def test_login(self):
        self.assertEqual(self.login_response_success.status_code, 302)
        self.assertEqual(self.login_response_fail.status_code, 200)


