from django.shortcuts import render, redirect
from django.http import HttpResponse
from review.models import Ulasan
from review import forms
from review.forms import FormUlasan
from .forms import LoginForm, SignUpForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.http import JsonResponse
import json

# Create your views here.
def ulasan(request):
    responses = {}
    if request.method == 'POST':
        if request.is_ajax():
            if request.user.is_authenticated:
                form = FormUlasan(request.POST)
                if form.is_valid():
                    name = request.user.username
                    message = form.cleaned_data['message']
                    Ulasan(name=name, message=message).save()
                    return JsonResponse({'success': True})
        return JsonResponse({'success': False})
    else:
        form = FormUlasan()
        responses['form'] = form
        return render(request, 'review.html', responses)

def get_JSON_testimonial(request):
    data = {
        'data':[i.get_dict() for i in Ulasan.objects.all()[::-1][:5]],
    }
    return JsonResponse(data=data)


def listReview (request):
    ulasan = Ulasan.objects.all().values()
    return render(request, 'stuff.html', {'ulasan': ulasan})


# Create your views here.
def logIn(request):
    if request.method == "POST":
        form = LoginForm(data = request.POST)

        if form.is_valid():
            usernameInput = request.POST["username"]
            passwordInput = request.POST["password"]

            print(usernameInput, passwordInput)
            user = authenticate(request, username = usernameInput, password = passwordInput)

            if user is not None:
                login(request, user) # A backend authenticated the credentials
                return redirect('review:login')

        else:
            messages.error(request, 'Invalid entry') # No backend authenticated the credentials

    else:
        form = LoginForm()

    context = {
        "form" : form,
    }

    return render(request, "login.html", context)

def signUp(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)

        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect("review:login")

        else:
            for msg in form.error_messages:
                messages.error(request, 'Invalid entry')

    else:
        form = SignUpForm()

    context = {
        'form' : form
    }

    return render(request, "signup.html", context)

def logOut(request):
    logout(request)
    return redirect("review:login")