from django.urls import path

from . import views

app_name = 'review'

urlpatterns = [
        path('ulasan', views.ulasan, name='ulasan'),
        path('listReview', views.listReview, name='listReview'),
        path('json/', views.get_JSON_testimonial, name='Ulasan_JSON'),        
        path('login/', views.logIn, name='login'),
        path('signup/', views.signUp, name='signup'),
        path('logout/', views.logOut, name='logout'),




]
