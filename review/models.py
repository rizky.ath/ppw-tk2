
from django.contrib.auth.models import User

from django.db import migrations, models 

# Create your models here.

class Ulasan(models.Model):
    name = models.CharField(max_length=50, blank=False, default="anonymous")
    time = models.DateTimeField(auto_now_add=True, blank=False)
    message = models.CharField(max_length=300, blank=False, default="")

    def get_dict(self):
        return {
            'name':self.name,
            'message':self.message,
            'time':self.time
        }
