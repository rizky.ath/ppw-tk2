from django.urls import path

from .views import idx, result_by_max_keyword, get_modelAPI

app_name = 'search'

urlpatterns = [
	path('resepapi/json/', get_modelAPI, name='model_json'),
	path('', idx, name='idx'),
	path('<str:pk>', result_by_max_keyword, name='res_max')
	
]
