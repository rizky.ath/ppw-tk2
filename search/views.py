from django.shortcuts import render, redirect
from add.models import Resep
from .models import Keyword
from django.db.models import F
from .forms import SearchForm
from django.core import serializers
from django.http import JsonResponse, HttpResponse
import json


def idx(request):
	allresep = Resep.objects.all() 
	allkeyword = Keyword.objects.all()
	if request.method=='POST':
		form = SearchForm(request.POST)
		if form.is_valid():
			keywordlist = form.cleaned_data
			keyword = keywordlist['string']
			for i in range(len(allkeyword)):
				if allkeyword[i].string.lower() == keyword.lower():
					allkeyword[i].freq = F('freq') + 1
					allkeyword[i].save(update_fields=['freq'])
					break
			else:
				form.save()

			list_resep_by_keyword = []
			for resep in allresep:
				if keyword.lower() in resep.nama.lower():
					list_resep_by_keyword.append(resep)
			context = {'allresep':list_resep_by_keyword, 
						'searchform' : SearchForm()}
			return render(request, 'searchApp/searchresult.html', context)

	form = SearchForm()
	most_searched_keyword = cari_max_freq_keyword(allkeyword)
	response = {'allresep': allresep, 'searchform': form, 'most_searched_keyword' : most_searched_keyword}
	return render(request, 'searchApp/searchresult.html', response)

def result_by_max_keyword(request, pk):
	max_keyword = Keyword.objects.get(id=pk)
	max_keyword.freq = F('freq') + 1
	max_keyword.save(update_fields=['freq'])

	allresep = Resep.objects.all()

	list_resep_by_keyword = []
	for resep in allresep:
		if max_keyword.string.lower() in resep.nama.lower():
			list_resep_by_keyword.append(resep)
	context = {'allresep':list_resep_by_keyword, 
				'searchform' : SearchForm()}
	return render(request, 'searchApp/searchresult.html', context)

def get_modelAPI(request):
	resep_model_text = serializers.serialize("json", Resep.objects.all())
	resep_model_json = json.loads(resep_model_text)
	context = {"resep_model_json" : resep_model_json}
	return JsonResponse(context)
	

def cari_max_freq_keyword(lst):
	if len(lst) > 0:
		maxi = lst[0]
		for i in range(1, len(lst)):
			if lst[i].freq > maxi.freq:
				maxi = lst[i]
		return maxi
	else:
		return None
