from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import Keyword
from add.models import Resep
from .views import idx
from .forms import SearchForm



# Create your tests here.
class SearchUnitTest(TestCase):
	def setUp(self):
		self.keyword = Keyword.objects.create(string='a')
		self.resep1 = Resep.objects.create(nama='pizza')
		self.resep2 = Resep.objects.create(nama='indomie')
		self.search_result = []

	def test_model_keyword_and_resep(self):
		self.assertEqual(Keyword.objects.count(), 1)
		self.assertEqual(Resep.objects.count(), 2)

	def test_index_search_existing_keyword_gives_right_output(self):
		response = Client().post('/search/', {'string':'a'})
		html_response = response.content.decode('utf-8')
		self.assertIn(self.resep1.nama, html_response)
		self.assertNotIn(self.resep2.nama, html_response)
		self.assertTemplateUsed(response, 'searchApp/searchresult.html')

	def test_index_search_missing_keyword_gives_right_output(self):
		response = Client().post('/search/', {'string':'o'})
		html_response = response.content.decode('utf-8')
		self.assertEqual(Keyword.objects.count(), 2)

	def test_pk_search_gives_right_output(self):
		response = Client().get('/search/'+str(self.keyword.id))
		html_response = response.content.decode('utf-8')
		self.assertIn(self.resep1.nama, html_response)
		self.assertNotIn(self.resep2.nama, html_response)
		self.assertTemplateUsed(response, 'searchApp/searchresult.html')



class FormUnitTest(TestCase):
	def test_form_is_valid(self):
		form_search = SearchForm(data = {'string':'search'})
		self.assertTrue(form_search.is_valid())

	def test_form_invalid(self):
		form_search = SearchForm(data = {})
		self.assertFalse(form_search.is_valid())


class ViewsUnitTest(TestCase):
	def setUp(self):
		self.keyword = Keyword.objects.create(string='abc')

	def test_url_index_search_is_exist(self):
		response = Client().get('/search/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'searchApp/searchresult.html')

	def test_url_pk_search_is_exist(self):
		response = Client().get('/search/' + str(self.keyword.id))
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'searchApp/searchresult.html')

	def test_POST_search(self):
		response = Client().post('/search/',
										{
											'string' : 'abc'
										})
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'searchApp/searchresult.html')

	def test_get_jsonAPI(self):
		response = Client().get('/search/resepapi/json/')
		self.assertEqual(response.status_code, 200)




