from django import forms
from .models import Keyword

class SearchForm(forms.ModelForm):
	class Meta:
		model = Keyword
		fields = ['string']
		labels = {'string' : ''}
		widgets = {
            'string': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Search Recipes...',
                'size' : '70',
                'id' : 'input'
                }
            )
            }