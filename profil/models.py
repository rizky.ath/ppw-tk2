from django.db import models

# Create your models here.

class PostModel(models.Model):
	full_name 		= models.CharField(max_length=200)
	description 	= models.TextField(max_length = 10000)
	picture 		= models.ImageField(null=True, blank=True)


	def __str__(self):
		return self.full_name




