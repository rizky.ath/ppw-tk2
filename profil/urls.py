from django.urls import path

from profil.views import index, profile
from profil import views

app_name = 'profil'

urlpatterns = [
	path('', profile, name='profile'),
	path('addprofile/', index, name='index'),
	#path('view/', view, name='view')
    
]
