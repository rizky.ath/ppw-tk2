from django.shortcuts import render
from .forms import PostForm
from .models import PostModel
from django.http import HttpResponseRedirect

# Create your views here.

def profile(request):
	posts = PostModel.objects.all()
	context = {
		'page_title':'Chef Profile',
		'posts':posts,
	}

	return render(request, 'profile.html', context)

def index(request):
	post_form = PostForm(request.POST or None)
	if request.method == "POST":
		if post_form.is_valid():
			post_form.save()

			return HttpResponseRedirect("/profil/")

	context = {
		'page_title':'Add Profile',
		'post_form':post_form
	}

	return render(request, 'forms.html', context)

"""
def view(request):
	view_post = PostModel
	context = {
		'view_post':view_post,
	}

	return render(request, 'view.html', context)
"""